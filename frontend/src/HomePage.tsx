/** @jsxImportSource @emotion/react */
import { css } from '@emotion/react';
import React from 'react';
import { QuestionList } from './QuestionList';
import { getUnansweredQuestions } from './QuestionsData';
import { PageTitle } from './PageTitle';
import { Page } from './Page';
import { PrimaryButton } from './Styles';
//import { Question } from './Question';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {
  gettingUnansweredQuestionsAction,
  gotUnansweredQuestionsAction,
  AppState,
} from './Store';

export const HomePage = () => {
  const dispatch = useDispatch();
  const questions = useSelector(
    (state: AppState) => state.questions.unanswered,
  );
  const questionsLoading = useSelector(
    (state: AppState) => state.questions.loading,
  );
  // const [questions, setQuestions] = React.useState<QuestionData[]>([]);
  // const [questionsLoading, setQuestionsLoading] = React.useState(true);
  const navigate = useNavigate();
  const handleAskQuestionClick = () => {
    console.log('TODO - move to the AskPage');
    navigate('ask');
  };
  React.useEffect(() => {
    const doGetUnansweredQuestions = async () => {
      dispatch(gettingUnansweredQuestionsAction());
      const unansweredQuestions = await getUnansweredQuestions();
      dispatch(gotUnansweredQuestionsAction(unansweredQuestions));
      // setQuesetQuestions(unansweredQuestions);
      // stionsLoading(false);
    };
    doGetUnansweredQuestions();
    //es lint-disable-next-line react
    //hooks / exhaustive - deps;
    // eslint-disable-next-line
  }, []);
  return (
    <Page>
      <div
        css={css`
          display: flex;
          align-items: center;
          justify-content: space-between;
        `}
      >
        <PageTitle>Unanswered Questions</PageTitle>
        <PrimaryButton onClick={handleAskQuestionClick}>
          Ask a question
        </PrimaryButton>
      </div>
      {questionsLoading ? (
        <div>Loading...</div>
      ) : (
        <QuestionList
          data={questions}
          // renderItem={(question) => (
          //   // <div>{question.title}</div>
          //   <Question>question</Question>
          // )}
        />
      )}
    </Page>
  );
};
